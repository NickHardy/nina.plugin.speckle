﻿<!--
    Copyright © 2016 - 2021 Stefan Berg <isbeorn86+NINA@googlemail.com> and the N.I.N.A. contributors

    This file is part of N.I.N.A. - Nighttime Imaging 'N' Astronomy.

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.-->
<ResourceDictionary
    x:Class="NINA.Plugin.Speckle.Sequencer.SequenceItem.DataTemplates"
    xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
    xmlns:converters="clr-namespace:NINA.Core.Utility.Converters;assembly=NINA.Core"
    xmlns:wpfutil="clr-namespace:NINA.WPF.Base.Utility;assembly=NINA.WPF.Base"
    xmlns:local="clr-namespace:NINA.Plugin.Speckle.Sequencer.SequenceItem"
    xmlns:mini="clr-namespace:NINA.View.Sequencer.MiniSequencer;assembly=NINA.Sequencer"
    xmlns:ninactrl="clr-namespace:NINACustomControlLibrary;assembly=NINACustomControlLibrary"
    xmlns:ns="clr-namespace:NINA.Core.Locale;assembly=NINA.Core"
    xmlns:util="clr-namespace:NINA.Core.Utility;assembly=NINA.Core"
    xmlns:rules="clr-namespace:NINA.Core.Utility.ValidationRules;assembly=NINA.Core"
    xmlns:view="clr-namespace:NINA.View.Sequencer;assembly=NINA.Sequencer">

    <ResourceDictionary.MergedDictionaries>
        <wpfutil:SharedResourceDictionary>
            <converters:BooleanToVisibilityCollapsedConverter x:Key="BooleanToVisibilityCollapsedConverter" />
            <converters:InverseBooleanToVisibilityCollapsedConverter x:Key="InverseBooleanToVisibilityCollapsedConverter" />
        </wpfutil:SharedResourceDictionary>
    </ResourceDictionary.MergedDictionaries>

    <WrapPanel
        x:Key="TakeExposureDetails"
        x:Shared="false"
        Orientation="Horizontal">
        <WrapPanel.Resources>
            <util:BindingProxy x:Key="CameraInfo" Data="{Binding CameraInfo}" />
        </WrapPanel.Resources>

        <TextBlock VerticalAlignment="Center" Text="{ns:Loc LblTime}" />
        <ninactrl:UnitTextBox
            MinWidth="40"
            Margin="5,0,0,0"
            VerticalAlignment="Center"
            VerticalContentAlignment="Center"
            Text="{Binding ExposureTime}"
            TextAlignment="Right"
            Unit="s" />

        <TextBlock Margin="5,0,0,0" VerticalAlignment="Center" Text="x" />
        <ninactrl:UnitTextBox
            MinWidth="20"
            Margin="5,0,0,0"
            VerticalAlignment="Center"
            VerticalContentAlignment="Center"
            Text="{Binding ExposureTimeMultiplier}"
            TextAlignment="Right" />

        <TextBlock
            Margin="7.5,0,7.5,0"
            HorizontalAlignment="Center"
            VerticalAlignment="Center"
            Text="|" />

        <TextBlock VerticalAlignment="Center" Text="{ns:Loc LblType}" />
        <ComboBox
            Margin="5,0,0,0"
            ItemsSource="{Binding ImageTypes}"
            SelectedItem="{Binding ImageType, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged}" />

        <TextBlock
            Margin="7.5,0,7.5,0"
            HorizontalAlignment="Center"
            VerticalAlignment="Center"
            Text="|" />

        <TextBlock VerticalAlignment="Center" Text="{ns:Loc LblBinning}" />
        <ComboBox
            Margin="5,0,0,0"
            DisplayMemberPath="Name"
            ItemsSource="{Binding Source={StaticResource CameraInfo}, Path=Data.BinningModes, Converter={StaticResource DefaultBinningModesConverter}}"
            SelectedItem="{Binding Binning, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged}"
            SelectedValuePath="Name" />

        <!--  List of Gain  -->
        <WrapPanel Orientation="Horizontal">
            <WrapPanel.Visibility>
                <PriorityBinding>
                    <Binding
                        Converter="{StaticResource CollectionContainsItemsToVisibilityConverter}"
                        Path="Data.Gains"
                        Source="{StaticResource CameraInfo}" />
                    <Binding
                        Converter="{StaticResource BooleanToVisibilityCollapsedConverter}"
                        Path="Data.Connected"
                        Source="{StaticResource CameraInfo}" />
                </PriorityBinding>
            </WrapPanel.Visibility>
            <TextBlock
                Margin="7.5,0,7.5,0"
                HorizontalAlignment="Center"
                VerticalAlignment="Center"
                Text="|" />
            <TextBlock VerticalAlignment="Center" Text="{ns:Loc LblGain}" />
            <ComboBox
                Margin="5,0,0,0"
                DisplayMemberPath="Text"
                IsSynchronizedWithCurrentItem="True"
                SelectedValuePath="Text">
                <ComboBox.ItemsSource>
                    <CompositeCollection>
                        <TextBlock Text="{Binding Source={StaticResource CameraInfo}, Path=Data.DefaultGain, UpdateSourceTrigger=PropertyChanged, StringFormat=({0})}" />
                        <CollectionContainer Collection="{Binding Source={StaticResource CameraInfo}, Path=Data.Gains, Converter={StaticResource IntListToTextBlockListConverter}}" />
                    </CompositeCollection>
                </ComboBox.ItemsSource>
                <ComboBox.SelectedValue>
                    <MultiBinding
                        Converter="{StaticResource MinusOneToBaseValueConverter}"
                        Mode="TwoWay"
                        UpdateSourceTrigger="PropertyChanged">
                        <Binding
                            Mode="TwoWay"
                            Path="Gain"
                            UpdateSourceTrigger="PropertyChanged" />
                        <Binding
                            Mode="OneWay"
                            Path="Data.DefaultGain"
                            Source="{StaticResource CameraInfo}"
                            UpdateSourceTrigger="PropertyChanged" />
                    </MultiBinding>
                </ComboBox.SelectedValue>
            </ComboBox>
        </WrapPanel>

        <!--  Free Gain  -->
        <WrapPanel Orientation="Horizontal">
            <WrapPanel.Visibility>
                <PriorityBinding FallbackValue="Visible">
                    <Binding
                        Converter="{StaticResource InverseCollectionContainsItemsToVisibilityConverter}"
                        Path="Data.Gains"
                        Source="{StaticResource CameraInfo}" />
                </PriorityBinding>
            </WrapPanel.Visibility>
            <TextBlock
                Margin="7.5,0,7.5,0"
                HorizontalAlignment="Center"
                VerticalAlignment="Center"
                Text="|" />
            <TextBlock VerticalAlignment="Center" Text="{ns:Loc LblGain}" />
            <ninactrl:HintTextBox
                MinWidth="40"
                Margin="5,0,0,0"
                VerticalAlignment="Center"
                HorizontalContentAlignment="Right"
                VerticalContentAlignment="Center"
                Foreground="{StaticResource PrimaryBrush}"
                TextAlignment="Right">
                <ninactrl:HintTextBox.HintText>
                    <Binding
                        Converter="{StaticResource CameraDefaultValueConverter}"
                        Mode="OneWay"
                        Path="Data.DefaultGain"
                        Source="{StaticResource CameraInfo}"
                        UpdateSourceTrigger="PropertyChanged" />
                </ninactrl:HintTextBox.HintText>
                <ninactrl:HintTextBox.Text>
                    <Binding
                        Converter="{StaticResource MinusOneToEmptyStringConverter}"
                        Mode="TwoWay"
                        Path="Gain"
                        UpdateSourceTrigger="PropertyChanged">
                        <Binding.ValidationRules>
                            <util:ShortRangeRule>
                                <util:ShortRangeRule.ValidRange>
                                    <util:ShortRangeChecker Maximum="32767" Minimum="-1" />
                                </util:ShortRangeRule.ValidRange>
                            </util:ShortRangeRule>
                        </Binding.ValidationRules>
                    </Binding>
                </ninactrl:HintTextBox.Text>
            </ninactrl:HintTextBox>
        </WrapPanel>

        <!--  Offset  -->
        <WrapPanel Orientation="Horizontal">
            <WrapPanel.Visibility>
                <MultiBinding Converter="{StaticResource BooleanOrToVisibilityCollapsedMultiConverter}" FallbackValue="Visible">
                    <Binding
                        Converter="{StaticResource InverseBooleanConverter}"
                        Path="Data.Connected"
                        Source="{StaticResource CameraInfo}" />
                    <Binding Path="Data.CanSetOffset" Source="{StaticResource CameraInfo}" />
                </MultiBinding>
            </WrapPanel.Visibility>
            <TextBlock
                Margin="7.5,0,7.5,0"
                HorizontalAlignment="Center"
                VerticalAlignment="Center"
                Text="|" />
            <TextBlock VerticalAlignment="Center" Text="{ns:Loc LblOffset}" />
            <ninactrl:HintTextBox
                MinWidth="40"
                Margin="5,0,0,0"
                VerticalAlignment="Center"
                HorizontalContentAlignment="Right"
                VerticalContentAlignment="Center"
                Foreground="{StaticResource PrimaryBrush}"
                TextAlignment="Right">
                <ninactrl:HintTextBox.HintText>
                    <Binding
                        Converter="{StaticResource CameraDefaultValueConverter}"
                        Mode="OneWay"
                        Path="Data.DefaultOffset"
                        Source="{StaticResource CameraInfo}"
                        UpdateSourceTrigger="PropertyChanged" />
                </ninactrl:HintTextBox.HintText>
                <ninactrl:HintTextBox.Text>
                    <Binding
                        Converter="{StaticResource MinusOneToEmptyStringConverter}"
                        Mode="TwoWay"
                        Path="Offset"
                        UpdateSourceTrigger="PropertyChanged">
                        <Binding.ValidationRules>
                            <util:ShortRangeRule>
                                <util:ShortRangeRule.ValidRange>
                                    <util:ShortRangeChecker Maximum="32767" Minimum="-1" />
                                </util:ShortRangeRule.ValidRange>
                            </util:ShortRangeRule>
                        </Binding.ValidationRules>
                    </Binding>
                </ninactrl:HintTextBox.Text>
            </ninactrl:HintTextBox>
        </WrapPanel>
    </WrapPanel>

    <DataTemplate x:Key="NINA.Plugin.Speckle.Sequencer.SequenceItem.TakeRoiExposures_Mini">
        <mini:MiniSequenceItem>
            <mini:MiniSequenceItem.SequenceItemContent>
                <StackPanel Orientation="Horizontal">
                    <TextBlock VerticalAlignment="Center" Text="{Binding ExposureCount}" />
                    <TextBlock VerticalAlignment="Center" Text="/" />
                    <TextBlock VerticalAlignment="Center" Text="{Binding TotalExposureCount}" />
                </StackPanel>
            </mini:MiniSequenceItem.SequenceItemContent>
        </mini:MiniSequenceItem>
    </DataTemplate>

    <DataTemplate DataType="{x:Type local:TakeRoiExposures}">
        <view:SequenceBlockView>
            <view:SequenceBlockView.SequenceItemContent>
                <WrapPanel Orientation="Horizontal">

                    <TextBlock VerticalAlignment="Center" Text="#" />
                    <TextBox
                        MinWidth="40"
                        Margin="5,0,0,0"
                        HorizontalAlignment="Center"
                        VerticalAlignment="Center"
                        Text="{Binding TotalExposureCount, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged}" />
                    <TextBlock
                        Margin="7.5,0,7.5,0"
                        HorizontalAlignment="Center"
                        VerticalAlignment="Center"
                        Text="|" />

                    <ContentPresenter Content="{StaticResource TakeExposureDetails}" />

                </WrapPanel>
            </view:SequenceBlockView.SequenceItemContent>
        </view:SequenceBlockView>
    </DataTemplate>

    <DataTemplate x:Key="NINA.Plugin.Speckle.Sequencer.SequenceItem.TakeLiveExposures_Mini">
        <mini:MiniSequenceItem>
            <mini:MiniSequenceItem.SequenceItemContent>
                <StackPanel Orientation="Horizontal">
                    <TextBlock VerticalAlignment="Center" Text="{Binding ExposureCount}" />
                    <TextBlock VerticalAlignment="Center" Text="/" />
                    <TextBlock VerticalAlignment="Center" Text="{Binding TotalExposureCount}" />
                </StackPanel>
            </mini:MiniSequenceItem.SequenceItemContent>
        </mini:MiniSequenceItem>
    </DataTemplate>

    <DataTemplate DataType="{x:Type local:TakeLiveExposures}">
        <view:SequenceBlockView>
            <view:SequenceBlockView.SequenceItemContent>
                <WrapPanel Orientation="Horizontal">

                    <TextBlock VerticalAlignment="Center" Text="#" />
                    <TextBox
                        MinWidth="40"
                        Margin="5,0,0,0"
                        HorizontalAlignment="Center"
                        VerticalAlignment="Center"
                        Text="{Binding TotalExposureCount, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged}" />
                    <TextBlock
                        Margin="7.5,0,7.5,0"
                        HorizontalAlignment="Center"
                        VerticalAlignment="Center"
                        Text="|" />

                    <ContentPresenter Content="{StaticResource TakeExposureDetails}" />
                    
                </WrapPanel>
            </view:SequenceBlockView.SequenceItemContent>
        </view:SequenceBlockView>
    </DataTemplate>

    <DataTemplate DataType="{x:Type local:CenterOnStarCluster}">
        <view:SequenceBlockView>
            <view:SequenceBlockView.SequenceItemContent>
                <StackPanel Orientation="Horizontal">
                    <TextBlock VerticalAlignment="Center" Text="Searchradius" />
                    <ninactrl:UnitTextBox
                        MinWidth="40"
                        Margin="5,0,0,0"
                        VerticalAlignment="Center"
                        VerticalContentAlignment="Center"
                        Text="{Binding SearchRadius}"
                        TextAlignment="Right"
                        Unit="°" />

                    <TextBlock
                        Margin="7.5,0,7.5,0"
                        HorizontalAlignment="Center"
                        VerticalAlignment="Center"
                        Text="|" />

                    <TextBlock
                            VerticalAlignment="Center"
                            Text="Search at zenith" />
                    <CheckBox
                            Width="80"
                            HorizontalAlignment="Right"
                            IsChecked="{Binding Zenith}">
                    </CheckBox>

                    <TextBlock
                        Margin="7.5,0,7.5,0"
                        HorizontalAlignment="Center"
                        VerticalAlignment="Center"
                        Text="|" />

                    <TextBlock
                            VerticalAlignment="Center"
                            Text="Platesolve" />
                    <CheckBox
                            Width="80"
                            HorizontalAlignment="Right"
                            IsChecked="{Binding Platesolve}">
                    </CheckBox>
                    <StackPanel Orientation="Horizontal" Margin="0"
                                Visibility="{Binding Platesolve, Converter={StaticResource BooleanToVisibilityCollapsedConverter}}">
                        <TextBlock
                        Margin="7.5,0,7.5,0"
                        HorizontalAlignment="Center"
                        VerticalAlignment="Center"
                        Text="|" />

                        <TextBlock
                            VerticalAlignment="Center"
                            Text="Slew back to target" />
                        <CheckBox
                            Width="80"
                            HorizontalAlignment="Right"
                            IsChecked="{Binding SlewBackToTarget}">
                        </CheckBox>
                    </StackPanel>
                </StackPanel>
            </view:SequenceBlockView.SequenceItemContent>
        </view:SequenceBlockView>
    </DataTemplate>

    <DataTemplate DataType="{x:Type local:SynchOnStarCluster}">
        <view:SequenceBlockView>
            <view:SequenceBlockView.SequenceItemContent>
                <StackPanel Orientation="Horizontal">
                    <TextBlock VerticalAlignment="Center" Text="Searchradius" />
                    <ninactrl:UnitTextBox
                        MinWidth="40"
                        Margin="5,0,0,0"
                        VerticalAlignment="Center"
                        VerticalContentAlignment="Center"
                        Text="{Binding SearchRadius}"
                        TextAlignment="Right"
                        Unit="°" />

                    <TextBlock
                        Margin="7.5,0,7.5,0"
                        HorizontalAlignment="Center"
                        VerticalAlignment="Center"
                        Text="|" />

                    <TextBlock
                            VerticalAlignment="Center"
                            Text="Slew back to target" />
                    <CheckBox
                            Width="80"
                            HorizontalAlignment="Right"
                            IsChecked="{Binding SlewBackToTarget}">
                    </CheckBox>

                </StackPanel>
            </view:SequenceBlockView.SequenceItemContent>
        </view:SequenceBlockView>
    </DataTemplate>

    <DataTemplate x:Key="NINA.Plugin.Speckle.Sequencer.SequenceItem.TakeSingleExposure_Mini">
        <mini:MiniSequenceItem>
            <mini:MiniSequenceItem.SequenceItemContent>
                <StackPanel Orientation="Horizontal">
                    <TextBlock VerticalAlignment="Center" Text="{Binding ExposureTime}" />
                    <TextBlock VerticalAlignment="Center" Text="s" />
                </StackPanel>
            </mini:MiniSequenceItem.SequenceItemContent>
        </mini:MiniSequenceItem>
    </DataTemplate>

    <DataTemplate DataType="{x:Type local:TakeSingleExposure}">
        <view:SequenceBlockView>
            <view:SequenceBlockView.SequenceItemContent>
                <ContentPresenter Content="{StaticResource TakeExposureDetails}" />
            </view:SequenceBlockView.SequenceItemContent>
        </view:SequenceBlockView>
    </DataTemplate>

    <DataTemplate x:Key="NINA.Plugin.Speckle.Sequencer.SequenceItem.TakeSingleRoiExposure_Mini">
        <mini:MiniSequenceItem>
            <mini:MiniSequenceItem.SequenceItemContent>
                <StackPanel Orientation="Horizontal">
                    <TextBlock VerticalAlignment="Center" Text="{Binding ExposureTime}" />
                    <TextBlock VerticalAlignment="Center" Text="s" />
                </StackPanel>
            </mini:MiniSequenceItem.SequenceItemContent>
        </mini:MiniSequenceItem>
    </DataTemplate>

    <DataTemplate DataType="{x:Type local:TakeSingleRoiExposure}">
        <view:SequenceBlockView>
            <view:SequenceBlockView.SequenceItemContent>
                <ContentPresenter Content="{StaticResource TakeExposureDetails}" />
            </view:SequenceBlockView.SequenceItemContent>
        </view:SequenceBlockView>
    </DataTemplate>

    <DataTemplate DataType="{x:Type local:CalculateRoiPosition}">
        <view:SequenceBlockView>
            <view:SequenceBlockView.SequenceItemContent>
                <StackPanel Orientation="Horizontal">
                    <TextBlock
                            VerticalAlignment="Center"
                            Text="Image flipped: " />
                    <TextBlock
                            VerticalAlignment="Center"
                            Text="X" />
                    <CheckBox
                            Width="80"
                            HorizontalAlignment="Right"
                            IsChecked="{Binding ImageFlippedX}">
                    </CheckBox>

                    <TextBlock
                            VerticalAlignment="Center"
                            Text="Y" />
                    <CheckBox
                            Width="80"
                            HorizontalAlignment="Right"
                            IsChecked="{Binding ImageFlippedY}">
                    </CheckBox>

                </StackPanel>
            </view:SequenceBlockView.SequenceItemContent>
        </view:SequenceBlockView>
    </DataTemplate>

    <DataTemplate x:Key="NINA.Plugin.Speckle.Sequencer.SequenceItem.CalculateExposure_Mini">
        <mini:MiniSequenceItem>
            <mini:MiniSequenceItem.SequenceItemContent>
                <StackPanel Orientation="Horizontal">

                </StackPanel>
            </mini:MiniSequenceItem.SequenceItemContent>
        </mini:MiniSequenceItem>
    </DataTemplate>

    <DataTemplate DataType="{x:Type local:CalculateExposure}">
        <view:SequenceBlockView>
            <view:SequenceBlockView.SequenceItemContent>
                <StackPanel Orientation="Horizontal">

                </StackPanel>
            </view:SequenceBlockView.SequenceItemContent>
        </view:SequenceBlockView>
    </DataTemplate>

</ResourceDictionary>